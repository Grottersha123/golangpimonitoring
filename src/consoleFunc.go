package src

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"net/smtp"

	"crypto/tls"
	"database/sql"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/antchfx/xmlquery"
	"github.com/tamerh/xml-stream-parser"
)

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func AllChannels(doc *xmlquery.Node) {
	type EnvelopeAll struct {
		Body struct {
			CommunicationChannelQueryResponse struct {
				CommunicationChannelID []struct {
					ComponentID string `xml:"ComponentID" json:"Server"`
					ChannelID   string `xml:"ChannelID" json:"Name"`
				} `xml:"CommunicationChannelID"`
			} `xml:"CommunicationChannelQueryResponse" json:"CommunicationChannelQueryResponse"`
		} `xml:"Body"`
	}
	xml_body_all := fmt.Sprintf(`<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<SOAP-ENV:Header>
  <WSCorIDSOAPHeader xmlns="http://www.ca.com/apm" CorID="5DDCBCF10A781A120878537DD0477D84,1:1,0,1,podci|SAP Netweaver|POD_J00_server0|WebServices|Client|urn_defaultServiceNSDefaultService|Query,2,AgAAAYJIQgAAAAFGAAAAAQAAABFqYXZhLnV0aWwuSGFzaE1hcAAAAARIQgAAAAJGAAAAAgAAABBqYXZhLmxhbmcuU3RyaW5nAApUeG5UcmFjZUlkSEIAAAADRQAAAAIAJDVEREFENTczMEE3ODFBMTIwODc4NTM3REYzRkRFQzBCMTY0NEhCAAAABEUAAAACABNDYWxsZXIgQ29tcG9uZW50IElESEIAAAAFRQAAAAIAATFIQgAAAAZFAAAAAgAPQ2FsbGVyVGltZXN0YW1wSEIAAAAHRQAAAAIADTE1NjkyMzc4MTA0MTdIQgAAAAhFAAAAAgARVXBzdHJlYW1HVUlEQ2FjaGVIQgAAAAlGAAAAAwAAABNqYXZhLnV0aWwuQXJyYXlMaXN0AAAAAkhCAAAACkUAAAACACA1RERDOUY0RDBBNzgxQTEyMDg3ODUzN0REMjgxRTFDNEhCAAAAC0UAAAACACA1RERDOUY0RDBBNzgxQTEyMDg3ODUzN0REMjgxRTFDNA=="/>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
  <yq1:CommunicationChannelQueryRequest xmlns:yq1="http://sap.com/xi/BASIS"/>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>`)
	channel := xmlquery.FindOne(doc, "//channel")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")
	url_channels := host + channel.SelectAttr("url")
	username := channel.SelectAttr("username")
	password := channel.SelectAttr("password")

	resp, status, err := getRequest(username, password, url_channels, xml_body_all)

	data := []byte(resp)

	if err != nil {
		log.Fatal("Error on unmarshaling xml. ", err.Error())
	}
	var channels EnvelopeAll
	err = xml.Unmarshal(data, &channels)
	jsonData, _ := json.MarshalIndent(channels.Body.CommunicationChannelQueryResponse.CommunicationChannelID, "", "\t")
	fmt.Printf("%s", jsonData)
	if status != 200 {
		fmt.Println("500 система не отвечает")
	}
}

type ChannelStat struct {
	ChannelStatus []struct {
		Name    string `xml:"name,attr" json:"Name"`
		Service string `xml:"service,attr" json:"Service"`
		Party   string `xml:"party,attr" json:"Party" `
		Status  []struct {
			NodeName         string `xml:"nodeName,attr" json:"nodeName"`
			NodeNumber       string `xml:"" json:"NodeNumber"`
			State            string `xml:"state,attr" json:"State"`
			ActivationState  string `xml:"activationState,attr" json:"ActivationState"`
			AutomationStatus string `xml:"automationStatus" json:"AutomationStatus"`
		} `xml:"status" json:"Status"`
	} `xml:"channelStatus" json:"ChannelStatus"`
}

func GetChannels(doc *xmlquery.Node, variants []string) ChannelStat {
	channel := xmlquery.FindOne(doc, "//options")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")

	url := host + channel.SelectAttr("url")
	//fmt.Println(url)
	username := channel.SelectAttr("username")
	//fmt.Println(username)
	password := channel.SelectAttr("password")
	//fmt.Println(password)
	var status ChannelStat
	for _, variant := range variants {
		for _, n := range xmlquery.Find(doc, fmt.Sprintf(`//options//variant[@name="%s"]`, variant)) {
			for _, next := range n.SelectElements("channel") {
				v := GetStatusChannel(next.OutputXML(false), variant, username, password, url)
				status.ChannelStatus = append(status.ChannelStatus, v)
			}
		}
	}
	jsonData, _ := json.MarshalIndent(status, "", "\t")
	fmt.Printf("%s", jsonData)

	if status.ChannelStatus == nil {
		fmt.Println("500 система не отвечает или проверьте конфигурацию")
	}
	return status
}

func getRequest(username string, password string, url string, xml_body string) (string, int, error) {
	payload := []byte(strings.TrimSpace(xml_body))
	httpMethod := "POST"
	req, err := http.NewRequest(httpMethod, url, bytes.NewReader(payload))

	if err != nil {
		log.Fatal("Error on creating request object. ", err.Error())
	}
	req.Header.Set("Content-type", "text/xml; charset=utf-8")
	req.SetBasicAuth(username, password)

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	client.Timeout = time.Second * 15

	res, err := client.Do(req)

	defer res.Body.Close()

	respBody, err := ioutil.ReadAll(res.Body)
	return string(respBody), res.StatusCode, err
}

type AdapterFrameworkData struct {
	ReceiverName  string `xml:"receiverName"`
	SenderName    string `xml:"senderName"`
	Count         int
	InterfaceName string `xml:"interface>name"`
}

type QueueData struct {
	Queue  []AdapterFrameworkData
	Number string
}
type ChannelData struct {
	Name    string
	Service string
	Uuid    string
	Status string
	Date    string
}

func unique(intSlice []struct {
	ReceiverName  string `xml:"receiverName"`
	SenderName    string `xml:"senderName"`
	Count         int
	InterfaceName string `xml:"interface>name"`
}) []AdapterFrameworkData {
	keys := make(map[AdapterFrameworkData]bool)
	list := []AdapterFrameworkData{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func GetQueue(doc *xmlquery.Node, st_res string) {

	res := xmlquery.Find(doc, fmt.Sprintf(`//queque//status[@name="%s"]`, st_res))
	currentTime := res[0].SelectElement("currentTime").OutputXML(false)
	minutes, err := strconv.Atoi(res[0].SelectAttr("minutes"))

	var startTime string
	layout := "2006-01-02T15:04:05"
	if currentTime != "" {

		t, err := time.Parse(layout, currentTime)
		startTime = t.Add(time.Duration(minutes) * (-1) * time.Minute).Format(layout)
		if err != nil {
			fmt.Println("Broke Parser date", err)
		}
	}
	if currentTime == "" {
		currentTime = time.Now().Format(layout)
		t, err := time.Parse(layout, currentTime)
		startTime = t.Add(time.Duration(minutes) * (-1) * time.Minute).Format(layout)
		if err != nil {
			fmt.Println("Broke Parser date", err)
		}
	}
	if err != nil {
		fmt.Println(err)
	}

	queque := xmlquery.FindOne(doc, "//queque")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")
	url := host + queque.SelectAttr("url")
	status_name := res[0].SelectAttr("name")
	maxMess := queque.SelectAttr("maxMessage")
	username := queque.SelectAttr("username")
	password := queque.SelectAttr("password")

	xml_body_queque := fmt.Sprintf(`<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<SOAP-ENV:Header>
  <WSCorIDSOAPHeader xmlns="http://www.ca.com/apm" CorID="675652060A781A120878537D71A50BA7,1:1,0,1,podci|SAP Netweaver|POD_J00_server0|WebServices|Client|urn_defaultServiceNSDefaultService|getMessageList,2,AgAAAYJIQgAAAAFGAAAAAQAAABFqYXZhLnV0aWwuSGFzaE1hcAAAAARIQgAAAAJGAAAAAgAAABBqYXZhLmxhbmcuU3RyaW5nAApUeG5UcmFjZUlkSEIAAAADRQAAAAIAJDY3NTNCRkUyMEE3ODFBMTIwODc4NTM3RDQyRDEwOUYzNTMyN0hCAAAABEUAAAACABNDYWxsZXIgQ29tcG9uZW50IElESEIAAAAFRQAAAAIAATFIQgAAAAZFAAAAAgAPQ2FsbGVyVGltZXN0YW1wSEIAAAAHRQAAAAIADTE1NjkzOTY3NzMzODJIQgAAAAhFAAAAAgARVXBzdHJlYW1HVUlEQ2FjaGVIQgAAAAlGAAAAAwAAABNqYXZhLnV0aWwuQXJyYXlMaXN0AAAAAkhCAAAACkUAAAACACA2NzQ1OTBENDBBNzgxQTEyMDg3ODUzN0RDRTM0OTJERUhCAAAAC0UAAAACACA2NDE3RjRGRjBBNzgxQTEyMDg3ODUzN0Q4NzUxMjVEMg=="/>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
  <pns:getMessageList xmlns:pns="urn:AdapterMessageMonitoringVi">
    <yq1:filter xmlns:yq1="urn:AdapterMessageMonitoringVi" xmlns:pns="urn:com.sap.aii.mdt.server.adapterframework.ws">
      <pns:archive>false</pns:archive>
      <pns:dateType>0</pns:dateType>
      <pns:fromTime>%s</pns:fromTime>
      <pns:nodeId>0</pns:nodeId>
      <pns:onlyFaultyMessages>false</pns:onlyFaultyMessages>
      <pns:retries>0</pns:retries>
      <pns:retryInterval>0</pns:retryInterval>
      <pns:status>%s</pns:status>
      <pns:timesFailed>0</pns:timesFailed>
      <pns:toTime>%s</pns:toTime>
      <pns:wasEdited>false</pns:wasEdited>
    </yq1:filter>
    <pns:maxMessages>%s</pns:maxMessages>
  </pns:getMessageList>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>`, startTime, status_name, currentTime, maxMess)
	resp, status, err := getRequest(username, password, url, xml_body_queque)
	br := bufio.NewReaderSize(strings.NewReader(resp), 65536)
	parser := xmlparser.NewXMLParser(br, "rn7:AdapterFrameworkData")

	type AdapterFrameworkData struct {
		ReceiverName  string `xml:"receiverName"`
		SenderName    string `xml:"senderName"`
		Count         int
		InterfaceName string `xml:"interface>name"`
	}

	type QueueData struct {
		Queue  []AdapterFrameworkData
		Number int
	}
	keys := make(map[AdapterFrameworkData]bool)
	list := []AdapterFrameworkData{}
	list_temp := []AdapterFrameworkData{}
	var count int
	for xml := range parser.Stream() {
		// select books
		interfaceName := xml.Childs["rn7:interface"][0].Childs["rn2:name"][0].InnerText
		receiverName := xml.Childs["rn7:receiverName"][0].InnerText
		senderName := xml.Childs["rn7:senderName"][0].InnerText
		res := AdapterFrameworkData{
			ReceiverName:  receiverName,
			SenderName:    senderName,
			Count:         0,
			InterfaceName: interfaceName,
		}
		if _, value := keys[res]; !value {
			keys[res] = true
			list = append(list, res)
			res.Count = 1
			count += 1
			list_temp = append(list_temp, res)
		} else {
			for i := range list_temp {
				if list_temp[i].InterfaceName == res.InterfaceName && list_temp[i].SenderName == res.SenderName && list_temp[i].ReceiverName == res.ReceiverName {
					// Found!
					list_temp[i].Count += 1
					count += 1
					break
				}
			}
		}
	}

	res1 := QueueData{
		Queue:  list_temp,
		Number: count,
	}
	jsonData, _ := json.MarshalIndent(res1, "", "\t")
	fmt.Printf("%s", jsonData)
	if status!= 200 {
		fmt.Println("500 система не отвечает")
	}
}

func SetAutomationStatus(doc *xmlquery.Node, name string, service string) int {
	channel := xmlquery.FindOne(doc, "//options")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")

	url := host + channel.SelectAttr("url")
	username := channel.SelectAttr("username")
	//fmt.Println(username)
	password := channel.SelectAttr("password")

	xml_body := fmt.Sprintf(`<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SOAP-ENV:Header>
    <WSCorIDSOAPHeader xmlns="http://www.ca.com/apm" CorID="920DD7BD0A781A1200963176BDBD345B,1:1,0,1,podci|SAP Netweaver|POD_J00_server0|WebServices|Client|urn_defaultServiceNSDefaultService|setChannelAutomationStatus,2,AgAAAa9IQgAAAAFGAAAAAQAAABFqYXZhLnV0aWwuSGFzaE1hcAAAAARIQgAAAAJGAAAAAgAAABBqYXZhLmxhbmcuU3RyaW5nAApUeG5UcmFjZUlkSEIAAAADRQAAAAIAJDkyMEI5M0MyMEE3ODFBMTIwMDk2MzE3NkU5NzQ2Q0QwMzI1NkhCAAAABEUAAAACABNDYWxsZXIgQ29tcG9uZW50IElESEIAAAAFRQAAAAIAATFIQgAAAAZFAAAAAgAPQ2FsbGVyVGltZXN0YW1wSEIAAAAHRQAAAAIADTE1NzAxMTM0NDM3NzNIQgAAAAhFAAAAAgARVXBzdHJlYW1HVUlEQ2FjaGVIQgAAAAlGAAAAAwAAABNqYXZhLnV0aWwuQXJyYXlMaXN0AAAAA0hCAAAACkUAAAACACA5MjBEQTBFQTBBNzgxQTEyMDA5NjMxNzY4QjE2QkY4OUhCAAAAC0UAAAACACA5MjBEODI4NDBBNzgxQTEyMDA5NjMxNzY4M0E1QTQyMUhCAAAADEUAAAACACA5MjBEM0I3QjBBNzgxQTEyMDA5NjMxNzY5RDhBMDY0Nw=="/>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <yq1:setChannelAutomationStatus xmlns:yq1="urn:com:sap:netweaver:pi:monitoring">
      <channels name="%s" service="%s" party=""/>
      <automationState>WEBSERVICE</automationState>
    </yq1:setChannelAutomationStatus>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>`, name, service)

	_, status, _ := getRequest(username, password, url, xml_body)
	return status
}

func startChannel(doc *xmlquery.Node, name string, service string) int {

	channel := xmlquery.FindOne(doc, "//options")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")
	url := host + channel.SelectAttr("url")

	username := channel.SelectAttr("username")
	password := channel.SelectAttr("password")

	xml_body := fmt.Sprintf(`<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SOAP-ENV:Header>
    <WSCorIDSOAPHeader xmlns="http://www.ca.com/apm" CorID="824C19070A781A120096317636D2393F,1:1,0,1,podci|SAP Netweaver|POD_J00_server0|WebServices|Client|urn_defaultServiceNSDefaultService|stopChannels,2,AgAAAupIQgAAAAFGAAAAAQAAABFqYXZhLnV0aWwuSGFzaE1hcAAAAARIQgAAAAJGAAAAAgAAABBqYXZhLmxhbmcuU3RyaW5nAApUeG5UcmFjZUlkSEIAAAADRQAAAAIAJDgyNDg2QkRDMEE3ODFBMTIwMDk2MzE3NkRGMDA5QjM3OTI1MEhCAAAABEUAAAACABNDYWxsZXIgQ29tcG9uZW50IElESEIAAAAFRQAAAAIAATFIQgAAAAZFAAAAAgAPQ2FsbGVyVGltZXN0YW1wSEIAAAAHRQAAAAIADTE1Njk4NDkwODgyNjNIQgAAAAhFAAAAAgARVXBzdHJlYW1HVUlEQ2FjaGVIQgAAAAlGAAAAAwAAABNqYXZhLnV0aWwuQXJyYXlMaXN0AAAACkhCAAAACkUAAAACACA4MjRCQUIxNTBBNzgxQTEyMDA5NjMxNzYwOURBOTA0MUhCAAAAC0UAAAACACA4MjRCQUIxNTBBNzgxQTEyMDA5NjMxNzYwOURBOTA0MUhCAAAADEUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAADUUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAADkUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAD0UAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAEEUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAEUUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAEkUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAE0UAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwMw=="/>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <yq1:startChannels xmlns:yq1="urn:com:sap:netweaver:pi:monitoring">
      <channel name="%s" service="%s" party=""/>
					  <language>EN</language>
    </yq1:startChannels>

  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>`, name, service)
	_, status, _ := getRequest(username, password, url, xml_body)
	return status
}

func stopChannel(doc *xmlquery.Node, name string, service string) int {

	channel := xmlquery.FindOne(doc, "//options")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")
	url := host + channel.SelectAttr("url")
	username := channel.SelectAttr("username")
	password := channel.SelectAttr("password")

	xml_body := fmt.Sprintf(`<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SOAP-ENV:Header>
    <WSCorIDSOAPHeader xmlns="http://www.ca.com/apm" CorID="824C19070A781A120096317636D2393F,1:1,0,1,podci|SAP Netweaver|POD_J00_server0|WebServices|Client|urn_defaultServiceNSDefaultService|stopChannels,2,AgAAAupIQgAAAAFGAAAAAQAAABFqYXZhLnV0aWwuSGFzaE1hcAAAAARIQgAAAAJGAAAAAgAAABBqYXZhLmxhbmcuU3RyaW5nAApUeG5UcmFjZUlkSEIAAAADRQAAAAIAJDgyNDg2QkRDMEE3ODFBMTIwMDk2MzE3NkRGMDA5QjM3OTI1MEhCAAAABEUAAAACABNDYWxsZXIgQ29tcG9uZW50IElESEIAAAAFRQAAAAIAATFIQgAAAAZFAAAAAgAPQ2FsbGVyVGltZXN0YW1wSEIAAAAHRQAAAAIADTE1Njk4NDkwODgyNjNIQgAAAAhFAAAAAgARVXBzdHJlYW1HVUlEQ2FjaGVIQgAAAAlGAAAAAwAAABNqYXZhLnV0aWwuQXJyYXlMaXN0AAAACkhCAAAACkUAAAACACA4MjRCQUIxNTBBNzgxQTEyMDA5NjMxNzYwOURBOTA0MUhCAAAAC0UAAAACACA4MjRCQUIxNTBBNzgxQTEyMDA5NjMxNzYwOURBOTA0MUhCAAAADEUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAADUUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAADkUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAD0UAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAEEUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAEUUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAEkUAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwM0hCAAAAE0UAAAACACA4MjQ3Qjg0RTBBNzgxQTEyMDA5NjMxNzYyMjQ5NDEwMw=="/>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <yq1:stopChannels xmlns:yq1="urn:com:sap:netweaver:pi:monitoring">
      <channel name="%s" service="%s" party=""/>
					  <language>EN</language>
    </yq1:stopChannels>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>`, name, service)

	_, status, _ := getRequest(username, password, url, xml_body)
	return status
}

type Envelope struct {
	Body struct {
		GetChannelAutomationStatusResponse struct {
			ChannelStatus struct {
				Name    string `xml:"name,attr" json:"Name"`
				Service string `xml:"service,attr" json:"Service"`
				Party   string `xml:"party,attr" json:"Party" `
				Status  []struct {
					NodeName         string `xml:"nodeName,attr" json:"nodeName"`
					NodeNumber       string `xml:"" json:"NodeNumber"`
					State            string `xml:"state,attr" json:"State"`
					ActivationState  string `xml:"activationState,attr" json:"ActivationState"`
					AutomationStatus string `xml:"automationStatus" json:"AutomationStatus"`
				} `xml:"status" json:"Status"`
			} `xml:"channelStatus" json:"ChannelStatus"`
		} `xml:"getChannelAutomationStatusResponse" json:"ChannelStatus"`
	} `xml:"Body"`
}

func GetStatusChannel(name string, service string, username string, password string, url string) (struct {
	Name    string `xml:"name,attr" json:"Name"`
	Service string `xml:"service,attr" json:"Service"`
	Party   string `xml:"party,attr" json:"Party" `
	Status  []struct {
		NodeName         string `xml:"nodeName,attr" json:"nodeName"`
		NodeNumber       string `xml:"" json:"NodeNumber"`
		State            string `xml:"state,attr" json:"State"`
		ActivationState  string `xml:"activationState,attr" json:"ActivationState"`
		AutomationStatus string `xml:"automationStatus" json:"AutomationStatus"`
	} `xml:"status" json:"Status"`
}) {
	xml_body_channel := fmt.Sprintf(`<?xml version="1.0" encoding="UTF-8"?>
									<SOAP-ENV:Envelope xmlns:xs="http://www.w3.org/2001/XMLSchema" 
									xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
									xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
									<SOAP-ENV:Body><yq1:getChannelAutomationStatus xmlns:yq1='urn:com:sap:netweaver:pi:monitoring'>
									<channels name='%s' service='%s' party=''>
									</channels></yq1:getChannelAutomationStatus></SOAP-ENV:Body></SOAP-ENV:Envelope>`,
		name, service)
	resp, _, _ := getRequest(username, password, url, xml_body_channel)
	var v Envelope
	data := []byte(resp)
	_ = xml.Unmarshal(data, &v)

	for index, status := range v.Body.GetChannelAutomationStatusResponse.ChannelStatus.Status {
		v.Body.GetChannelAutomationStatusResponse.ChannelStatus.Status[index].NodeNumber = strings.Split(status.NodeName, " ")[1]
		v.Body.GetChannelAutomationStatusResponse.ChannelStatus.Status[index].NodeName = strings.Split(status.NodeName, " ")[2]
	}
	return v.Body.GetChannelAutomationStatusResponse.ChannelStatus
}

func SaveInfErr(db *sql.DB, uuid string, name string, service string, date string, status string) {
	_, err := db.Exec("INSERT INTO error_channels VALUES($1, $2, $3, $4,$5)", uuid, name, service, date, status)
	CheckErr(err)
}

func SaveResChan(db *sql.DB, uuid string, name string, service string, date string) {
	_, err := db.Exec("INSERT INTO restart_channels VALUES($1, $2, $3, $4)", uuid, name, service, date)
	CheckErr(err)
}

func RestartChannels(uuid string, db *sql.DB, doc *xmlquery.Node, ) [] ChannelData {
	rows, err := db.Query("SELECT * FROM channels WHERE uuid=?", uuid)
	CheckErr(err)
	var channels_restart []ChannelData
	var name string
	var service string
	var action string
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&name, &service, &uuid)
		fmt.Println(uuid)
		actions, err := db.Query("SELECT action FROM actions WHERE uuid=?", uuid)
		CheckErr(err)
		defer rows.Close()
		for actions.Next() {
			channels_restart = append(channels_restart, ChannelData{
				Name:    name,
				Service: service,
				Uuid:    uuid,
				Date:    time.Now().String(),
			})
			err = actions.Scan(&action)
			action := strings.Split(action, " ")
			for _, a := range action {
				a := strings.Trim(a, "\n")
				switch a {
				case "stop":
					stopChannel(doc, name, service)
				case "start":
					startChannel(doc, name, service)
				default:
					if a != "" {
						sec, err := strconv.Atoi(a)
						time.Sleep(time.Duration(sec) * time.Millisecond)
						CheckErr(err)
					}
				}
			}
		}
		actions.Close()
	}
	rows.Close()
	return channels_restart
}
func SendMail(addr, from, subject, body string, to []string) error {
	r := strings.NewReplacer("\r\n", "", "\r", "", "\n", "", "%0a", "", "%0d", "")
	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()
	if err = c.Mail(r.Replace(from)); err != nil {
		return err
	}
	for i := range to {
		to[i] = r.Replace(to[i])
		if err = c.Rcpt(to[i]); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	msg := "To: " + strings.Join(to, ",") + "\r\n" +
		"From: " + from + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"Content-Type: text/html; charset=\"UTF-8\"\r\n" +
		"Content-Transfer-Encoding: base64\r\n" +
		"\r\n" + base64.StdEncoding.EncodeToString([]byte(body))
	fmt.Println(msg)
	_, err = w.Write([]byte(msg))
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}


func Send(body string,subject string, doc *xmlquery.Node) {
	from := doc.SelectElement("/config/mail/emailfrom").OutputXML(false)
	//pass := "6412121995q"
	to :=  doc.SelectElement("/config/mail/emailto").OutputXML(false)
	server:= doc.SelectElement("/config/mail/server").OutputXML(false)
	pass := doc.SelectElement("/config/mail/pass").OutputXML(false)
	port := doc.SelectElement("/config/mail/port").OutputXML(false)
	fmt.Println(from, to, server, pass, port)
	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject +"\n\n" +
		body

	err := smtp.SendMail(server +":"+port,
		smtp.PlainAuth("", from, pass, server),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}

	//log.Print("sent, visit http://foobarbazz.mailinator.com")
}
//[] ChannelData
func RestartChannelsXml(uuid string, name string, service string, username string, password string, url string, doc *xmlquery.Node, ) int {

	actions_id := xmlquery.FindOne(doc,fmt.Sprintf("//config//options//action[id='%s']", uuid))
	var status int
	if actions_id.SelectElement("stop").OutputXML(false) != "" {
		min, _ := strconv.Atoi(actions_id.SelectElement("stop").OutputXML(false))
		someLHS := time.Duration(min)
		time.Sleep(someLHS * time.Second)
		fmt.Println("stop")
		time.Sleep(someLHS * time.Second)
		stopChannel(doc, name, service)
	}
	if actions_id.SelectElement("sleep").OutputXML(false) != "" {
		fmt.Println("sleep")
		sec, _ := strconv.Atoi(actions_id.SelectElement("sleep").OutputXML(false))
		someLHS := time.Duration(sec)
		time.Sleep(someLHS * time.Second)
	}
	if actions_id.SelectElement("start").OutputXML(false) != "" {
		fmt.Println("start")
		status = startChannel(doc, name, service)
		sec, _ := strconv.Atoi(actions_id.SelectElement("sleep").OutputXML(false))
		someLHS := time.Duration(sec)
		time.Sleep(someLHS * time.Second)
	}
	return status
	//rows, err := db.Query("SELECT * FROM channels WHERE uuid=?", uuid)
	//CheckErr(err)
	//var channels_restart []ChannelData
	//var name string
	//var service string
	//var action string
	//defer rows.Close()
	//for rows.Next() {
	//	err = rows.Scan(&name, &service, &uuid)
	//	fmt.Println(uuid)
	//	actions, err := db.Query("SELECT action FROM actions WHERE uuid=?", uuid)
	//	CheckErr(err)
	//	defer rows.Close()
	//	for actions.Next() {
	//		channels_restart = append(channels_restart, ChannelData{
	//			Name:    name,
	//			Service: service,
	//			Uuid:    uuid,
	//			Date:    time.Now().String(),
	//		})
	//		err = actions.Scan(&action)
	//		action := strings.Split(action, " ")
	//		for _, a := range action {
	//			a := strings.Trim(a, "\n")
	//			switch a {
	//			case "stop":
	//				stopChannel(doc, name, service)
	//			case "start":
	//				startChannel(doc, name, service)
	//			default:
	//				if a != "" {
	//					sec, err := strconv.Atoi(a)
	//					time.Sleep(time.Duration(sec) * time.Millisecond)
	//					CheckErr(err)
	//				}
	//			}
	//		}
	//	}
	//	actions.Close()
	//}
	//rows.Close()
	//return channels_restart
}
