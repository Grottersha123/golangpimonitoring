package main

import (
	"consoleFunc.go/src"
	"database/sql"
	"flag"
	"fmt"
	"github.com/antchfx/xmlquery"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/http" // пакет для поддержки HTTP протокола
	"os"
	"strconv"
	"strings" // пакет для работы с  UTF-8 строками
	"time"
)

func initilization(doc *xmlquery.Node, db *sql.DB) {

	var delete_tables = [] string{
		"channels",
		"error_channels",
		"restart_channels",
		"actions",
	}
	for _, data := range delete_tables {
		delete_tables_query, _ := db.Prepare(fmt.Sprintf("DELETE FROM %s", data))
		_, err := delete_tables_query.Exec()
		src.CheckErr(err)
	}
	insert_channel, err := db.Prepare("INSERT INTO channels(name, service, uuid) values(?,?,?)")
	insert_actions, err := db.Prepare("INSERT INTO actions(uuid, action, sec) values(?,?,?)")
	src.CheckErr(err)
	result := doc.SelectElements("//config//options//variant")
	actions_id := doc.SelectElements("//config//options//action//id")
	fmt.Println(actions_id[0], actions_id[0])
	for _, action := range actions_id {
		res_split := strings.TrimSpace(action.InnerText())
		sec := action.SelectAttr("sec")
		_, err := insert_actions.Exec(action.SelectAttr("name"), res_split, sec)
		src.CheckErr(err)
	}
	fmt.Println(result)
	for _, data := range result {
		for _, ch := range data.SelectElements("channel") {
			id := ch.SelectAttr("id")
			src.CheckErr(err)
			if ch.SelectAttr("name") == "" {
				_, err := insert_channel.Exec(ch.OutputXML(false), data.SelectAttr("name"), id)
				src.CheckErr(err)
			} else {
				_, err := insert_channel.Exec(ch.OutputXML(false), ch.SelectAttr("name"), id)
				src.CheckErr(err)
			}
		}
	}

}

//func checkErr(err error) {
//	if err != nil {
//		panic(err)
//	}
//}

//func restart()  {
//
//}

//values := []string{"one", "two", "three"}

func restartVariantChannel(doc *xmlquery.Node, variants []string) {

	channel := xmlquery.FindOne(doc, "//options")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")

	url := host + channel.SelectAttr("url")
	username := channel.SelectAttr("username")
	password := channel.SelectAttr("password")
	var channels_rest int
	//var channels_err [] src.ChannelData
	//var channels_rest [] src.ChannelData

	for _, variant := range variants {
		for _, n := range xmlquery.Find(doc, fmt.Sprintf(`//options//variant[@name="%s"]`, variant)) {
			for _, next := range n.SelectElements("channel") {
				data := src.GetStatusChannel(next.OutputXML(false), variant, username, password, url)
				if data.Status == nil  {
					fmt.Println("500 система не отвечает")
					return
				}
				for _, stat := range data.Status {
					if stat.AutomationStatus != "WEBSERVICE" {
						src.SetAutomationStatus(doc, data.Name, data.Service)
					}
					if stat.State == "UNKNOWN" || stat.State == "INACTIVE" && stat.ActivationState == "STOPPED" {
						fmt.Println("Attention the channel is stopped")
						var restart_channel string
						for _, restart := range xmlquery.Find(doc, fmt.Sprintf(`//options//variant[@name="%s"]`, variant)) {
							for _, next_res := range restart.SelectElements("channel") {
								fmt.Println(next_res.SelectAttr("id"), next_res.OutputXML(false), variant)
								channels_rest = src.RestartChannelsXml(next_res.SelectAttr("id"), next_res.OutputXML(false), variant, username, password, "", doc)
								if channels_rest == 200 {
									//src.SaveResChan(db, next_res.SelectAttr("id"), next_res.OutputXML(false), variant, strings.Split(time.Now().String(),".")[0])

									restart_status := src.GetStatusChannel(next_res.OutputXML(false), variant, username, password, url)
									restart_channel = restart_channel + "  " + next_res.OutputXML(false) + "  " + variant + "  " + restart_status.Status[0].ActivationState + "  " + strings.Split(time.Now().String(), ".")[0] + "\n"

								}
							}
						}
						src.Send(restart_channel, "SUCCESS channel", doc)
						fmt.Println(data.Name, data.Service)
						restart_error_status := src.GetStatusChannel(data.Name, data.Service, username, password, url)
						for index, error := range restart_error_status.Status {
							if error.State != "OK" {
								fmt.Println("ALARMMMMMMMMMMM!!!")
								body := restart_error_status.Name + " " + restart_error_status.Service + " " + error.ActivationState + " " + strings.Split(time.Now().String(), ".")[0]
								fmt.Println(strings.Split(time.Now().String(), ".")[0])
								src.Send(body, "ERROR channel", doc)
								break
							} else if index == 0 && error.State == "OK" {
								//src.SaveInfErr(db,  next.SelectAttr("id"), restart_error_status.Name, restart_error_status.Service,time.Now().String(),restart_error_status.Status[0].State )
							}
						}
						break
					}
				}
			}
		}
	}

}

func monitorChannels(doc *xmlquery.Node, db *sql.DB) {
	rows, err := db.Query("SELECT * from channels WHERE")
	src.CheckErr(err)

	defer rows.Close()
	var name string
	var uuid string
	var service string
	channel := xmlquery.FindOne(doc, "//options")
	host := xmlquery.FindOne(doc, "//config").SelectAttr("host")

	url := host + channel.SelectAttr("url")
	username := channel.SelectAttr("username")
	password := channel.SelectAttr("password")
	var channels_err [] src.ChannelData
	var channels_rest [] src.ChannelData
	for rows.Next() {
		err = rows.Scan(&name, &service, &uuid)
		data := src.GetStatusChannel(name, service, username, password, url)
		for _, stat := range data.Status {
			if stat.State == "UNKNOWN" || stat.State == "INACTIVE" && stat.ActivationState == "STOPPED" {
				if stat.AutomationStatus != "WEBSERVICE" {
					src.SetAutomationStatus(doc, data.Name, data.Service)
				}
				channels_err = append(channels_err, src.ChannelData{
					Name:    name,
					Service: service,
					Uuid:    uuid,
					Date:    time.Now().String(),
				})
				channels_rest = src.RestartChannels(uuid, db, doc)

			}
		}
	}
	src.CheckErr(err)
	rows.Close()
	//for _, e := range channels_err {
	//	src.SaveInfErr(db, e.Uuid, e.Name, e.Service, e.Date)
	//}
	for _, e := range channels_rest {
		src.SaveResChan(db, e.Uuid, e.Name, e.Service, e.Date)
	}
}

func oper() {
	read_xml := flag.String("r", "config.xml", "загрузка настроек из файла <path>")
	operation := flag.String("oper", "all",
		"1) all - вывод всех каналов \n"+
			"2) mntr - мониторинг каналов из конфигурационного файла \n"+
			"3) queue - мониторинг очередей \n"+
			"4) robot_restart - включить перезагрузку каналов \n")

	variant := flag.String("var", "PNTZ_KIS_D", "варианты: \n"+
		"PNTZ_KIS_D \n"+
		"CHTPZ_KIS_D \n"+
		"WEBTUTOR_D \n")

	status := flag.String("status", "", "статус: \n"+
		"delivering \n"+
		"systemError \n"+
		"holding \n")

	flag.Parse()

	xmlFile, _ := os.Open(*read_xml)
	doc, _ := xmlquery.Parse(xmlFile)

	switch *operation {
	case "all":
		src.AllChannels(doc)
	case "mntr":
		if *variant != "" {
			temp := flag.Args()
			temp = append(temp, *variant)
			src.GetChannels(doc, temp)
		}
	case "queue":
		if *status != "" {
			src.GetQueue(doc, *status)
		} else {
			fmt.Println("Введите команду queue -status <название статуса>")
		}
	case "robot_restart":
		if *variant != "" {
			temp := flag.Args()
			temp = append(temp, *variant)
			restartVariantChannel(doc, temp)
		}

	case "robot":
		db, _ = sql.Open("sqlite3", "./channels.db")
		db.SetMaxOpenConns(5)
		initilization(doc, db)
		go startPolling1(doc, db)

		http.HandleFunc("/", handler)
		http.HandleFunc("/restart", handlerRestart)
		http.HandleFunc("/error", handlerError)
		http.ListenAndServe(":8080", nil)

	case "restart":
		db, _ = sql.Open("sqlite3", "./channels.db")
		db.SetMaxOpenConns(5)
		initilization(doc, db)

	default:
		fmt.Printf("%s.\n", "Неправильно введена операция")
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func startPolling1(doc *xmlquery.Node, db *sql.DB) {
	min, _ := strconv.Atoi(xmlquery.FindOne(doc, "config//options").SelectAttr("minutes"))
	someLHS := time.Duration(min)
	for {
		time.Sleep(someLHS * time.Second)
		monitorChannels(doc, db)
	}
}
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

var db *sql.DB

func handlerRestart(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	start, err := r.URL.Query()["start_date"]
	end, err := r.URL.Query()["end_date"]
	if !err || len(start) < 1 || len(end) < 1 {
		log.Println("Url Param 'key' is missing")
		log.Println(start, end)
		return
	}
	rows, err1 := db.Query(fmt.Sprintf("select *  from restart_channels WHERE date BETWEEN '%s' and '%s'", start[0], end[0]))
	src.CheckErr(err1)
	defer rows.Close()

	chs := make([]*src.ChannelData, 0)
	for rows.Next() {
		ch := new(src.ChannelData)
		err := rows.Scan(&ch.Uuid, &ch.Name, &ch.Service, &ch.Date)
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		chs = append(chs, ch)
	}
	if err := rows.Err(); err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Restart channels \n")
	for _, ch := range chs {
		fmt.Fprintf(w, "Name: %s, Service: %s, Date: %s \n", ch.Name, ch.Service, strings.Split(ch.Date, " ")[0])
	}
}

func handlerError(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	start, err := r.URL.Query()["start_date"]
	end, err := r.URL.Query()["end_date"]
	if !err || len(start) < 1 || len(end) < 1 {
		log.Println("Url Param 'key' is missing")
		log.Println(start, end)
		return
	}
	rows, err1 := db.Query(fmt.Sprintf("select * from error_channels WHERE date BETWEEN '%s' and '%s'", start[0], end[0]))
	src.CheckErr(err1)
	defer rows.Close()

	chs := make([]*src.ChannelData, 0)
	for rows.Next() {
		ch := new(src.ChannelData)
		err := rows.Scan(&ch.Uuid, &ch.Name, &ch.Service, &ch.Date)
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		chs = append(chs, ch)
	}
	if err := rows.Err(); err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Error channels \n")
	for _, ch := range chs {
		fmt.Fprintf(w, "Name: %s, Service: %s, Date: %s \n", ch.Name, ch.Service, strings.Split(ch.Date, " ")[0])
	}
}

func main() {
	//xmlFile, _ := os.Open("config.xml")
	//doc, _ := xmlquery.Parse(xmlFile)
	//to :=  doc.SelectElement("/config/email").OutputXML(false)
	//fmt.Println(to)
	//db, _ = sql.Open("sqlite3", "./channels.db")
	//src.Send("Привет, я робот","проверка связи", doc)
	//src.SendMail("m.chtpz.ru:25", (&mail.Address{"from name", "fromhere@example.com"}).String(), "Email Subject", "Test.", []string{(&mail.Address{"to name", to}).String()})

	// Connect to the remote SMTP server.
	//c, err := smtp.Dial("m.chtpz.ru:25")
	//if err != nil {
	//	log.Fatal(err)
	//}
	//defer c.Close()
	//// Set the sender and recipient.
	//c.Mail("sender@example.org")
	//c.Rcpt(to)
	//// Send the email body.
	//wc, err := c.Data()
	//if err != nil {
	//	log.Fatal(err)
	//}
	//defer wc.Close()
	//buf := bytes.NewBufferString("This is the email body.")
	//if _, err = buf.WriteTo(wc); err != nil {
	//	log.Fatal(err)
	//}

	//src.RestartChannelsXml("37ac7188-925f-4bf4-9279-554be596f6cf", "JMS_Receiver_AccountingReserve", "CHTPZ_KIS_D", "AESILAYEVA", "6412121995q", "", doc, )
	//values := []string{"one", "two", "three"}
	//values := []string{"PNTZ_KIS_D"}
	//
	//restartVariantChannel(doc, values)
	//src.Send("Jsn down", "ERROR channel",doc)

	//initilization(doc, db)
	//	//var res []string
	//	//res = []string {"CHTPZ_KIS_D", "PNTZ_KIS_D"}
	//	//m.GetChannels(doc, res)
	oper()
}
